<?php

/**
 * @author JMColeman <hyperclock@voyatrax.eu> | original: Oleg Khussainov <oleg@hireoleg.com>
 * @package NivoSliderBundle
 *
 */

namespace Vtx\NivoSliderBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * vtxNivoSliderBundle
 *
 */
class VtxNivoSliderBundle extends Bundle
{

}
