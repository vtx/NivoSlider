jQuery Nivo Slider plugin integration for Symfony
=======================

**VtxNivoSliderBundle** is based on SfkNivoSliderBundle. 

We pulled it from git because we did not find one that worked well in Symfony3. I started to hardcode it into my system, 
but that's not what I wanted. 

The original _SfkNivoSliderBundle_ has not been updated in years, another point as to why we pulled a copy.
## Installation

### Add bundle via composer 

```bash
composer require vtx/nivo-slider-bundle:dev-master
```

### Register bundle in your application kernel

```php
// app/AppKernel.php
public function registerBundles() 
{
    $bundles = array(
        // ...
        new Vtx\NivoSliderBundle\VtxNivoSliderBundle(),
    );
}
```

### Install assets and clear cache

```bash
php bin/console assets:install --symlink
php bin/console cache:clear
```



### Include theme css file

```html
<link rel="stylesheet" href="{{ asset('bundles/vtxnivoslider/nivo-slider/themes/default/default.css') }}" type="text/css" media="screen" />
```

#### Include slider javascript file

>**NOTE**
>You must have jquery library included

```js
<script type="text/javascript" src="{{ asset('bundles/app/js/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bundles/vtxnivoslider/nivo-slider/jquery.nivo.slider.pack.js') }}"></script>
```

## License

Refer to the source code of the included files
